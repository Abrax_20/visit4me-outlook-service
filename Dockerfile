# TODO: Remove npm build Bug
FROM node:9.6.1

ARG DEV
ARG PORT

ENV PORT=$PORT
ENV source="."
ENV work="/home/visit4me"
ENV backend="backend"
ENV DEV=$DEV

EXPOSE $PORT

RUN mkdir ${work}
RUN cd ${work} && mkdir ${backend}

COPY ${source} ${work}/${backend}

WORKDIR ${work}/${backend}
RUN npm install -g yarn --silent
RUN yarn install --silent

#RUN npm install -g pm2

CMD (if $DEV; then npm start; else pm2 start --no-daemon processes.json; fi)
