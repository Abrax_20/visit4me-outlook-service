const fetch = require('node-fetch')

const OutlookConnection = require('./../models/outlook_connection/model');

const { GET_USER_ID_BY_AUTH_TOKEN } = require('./../config');

const checkAuthentication = async (req, res, next)  => {
    try {
        const token = req.headers['x-access-token'];
        if (!token) {
            req.userId = null;
            next();
            return;
        }

        const response = await fetch(GET_USER_ID_BY_AUTH_TOKEN, {
            headers: {
                'x-access-token': token,
                'Access-Type': 'application/json',
                'Content-Type': 'application/json',
            },
        });

        if (response.status !== 200) {
            req.userId = null;
            next();
            return;
        }

        const data = await response.json();

        if (!data.id || !data.success) {
            req.userId = null;
            next();
            return;
        }
        req.userId = data.id;
        next();
    } catch (e) {
        req.userId = null;
        next();
    }
};
const getOutlookUser = async (req, res, next) => {
    if (!req.userId) {
        res.status(400).json({ success: false, error: true, msg: 'bad request' });
        return;
    }

    const outlookUser = await OutlookConnection.findOne({ user_id: req.userId });


    if (!outlookUser) {
        res.status(400).json({ success: false, error: true, msg: 'bad request' });
        return;
    }

    req.outlookUser = outlookUser;
    next();
};
exports.getOutlookUser = getOutlookUser;
exports.checkAuthentication = checkAuthentication;
