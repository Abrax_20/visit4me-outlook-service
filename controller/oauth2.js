const { oauth2 } = require('./../helper/oauth2');

const checkAccessToken = async (req, res, next) => {
  try {
    const token = req.outlookUser.access_token;

    if (token) {
      const FIVE_MINUTES = 300000;
      const expiration = new Date(parseFloat(req.outlookUser.token_expires - FIVE_MINUTES));
      if (expiration > new Date()) {
        next();
        return;
      }
    }

    const refresh_token = req.outlookUser.refresh_token;
    if (refresh_token) {
      const response = await oauth2.accessToken.create({ refresh_token: refresh_token }).refresh();
      const newToken = response.token;
      req.outlookUser.access_token = newToken.access_token;
      req.outlookUser.token_expires = newToken.expires_at.getTime();
      await req.outlookUser.save();
      next();
      return;
    }

    res.status(500).json({ error: true, msg: 'internal server error' });
  } catch (e) {
    console.error(e);
    res.status(500).json({ error: true, msg: 'internal server error' });
  }
};
exports.checkAccessToken = checkAccessToken;
