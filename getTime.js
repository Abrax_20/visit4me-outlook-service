const date = new Date();
const localTime = date.getTime();
const localOffset = date.getTimezoneOffset() * 60000;
const utc = localTime + localOffset;
// http://momentjs.com/timezone/
