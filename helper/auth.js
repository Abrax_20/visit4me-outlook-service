const { oauth2 } = require('./oauth2');

const getAuthUrl = () => {
    const returnVal = oauth2.authorizationCode.authorizeURL({
        redirect_uri: process.env.REDIRECT_URI,
        scope: process.env.APP_SCOPES
    });
    return returnVal;
};
exports.getAuthUrl = getAuthUrl;

const getTokenFromCode = async (code, res) => {
  const result = await oauth2.authorizationCode.getToken({
    code,
    scope: process.env.APP_SCOPES,
    redirect_uri: process.env.REDIRECT_URI,
  });

  const response = oauth2.accessToken.create(result);
  return response.token;
};
exports.getTokenFromCode = getTokenFromCode;
