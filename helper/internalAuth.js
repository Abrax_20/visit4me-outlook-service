const fetch = require('node-fetch');

const { GET_USER_BY_AUTH_TOKEN } = require('./../config');

const refreshSalesforceUser = (req) => async (accessToken, res) => {
    try {
        if (req.sfUser) {
            req.sfUser.accessToken = accessToken;
            await req.sfUser.save();
        }
    } catch (e) {}
};
const getUserIdByAuth = async (token) => {
    try {
        const response = await fetch(GET_USER_BY_AUTH_TOKEN, {
            headers: {
                'x-access-token': token,
                'Content-Type': 'application/json',
                'Access-Type': 'application/json',
            },
        });

        if (response.status !== 200) {
            return null;
        }

        const data = await response.json();

        if (!data.id) {
            return null;
        }

        return data.id;
    } catch (e) {
        console.log(e);
        return null;
    }
};
const getToken = (req) => req.headers['x-access-token'];

exports.getToken = getToken;
exports.getUserIdByAuth = getUserIdByAuth;
exports.refreshSalesforceUser = refreshSalesforceUser;
