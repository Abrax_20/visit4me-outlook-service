const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const OutlookConnection = new Schema({
    user_id: {
        type: String,
        default: null,
        required: true,
    },
    company_id: {
        type: String,
        default: null,
        required: false,
    },
    access_token: {
        type: String,
        required: true,
    },
    refresh_token: {
        type: String,
        required: true,
    },
    token_expires: {
        type: String,
        required: true,
    },
    outlook_user_name: {
        type: String,
        required: true,
    },
    outlook_user_email: {
        type: String,
        required: true,
    }
});
module.exports = OutlookConnection;
