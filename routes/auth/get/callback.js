const jwt = require('jsonwebtoken');
const { getTokenFromCode } = require('../../../helper/auth');
const OutlookConnection = require('../../../models/outlook_connection/model');

const callback = async (req, res) => {
    try {
        const code = req.query.code;
        if (!code || !req.session.userId) {
            res.redirect(process.env.ERROR_CALLBACK_URL);
            return;
        }
        const tokens = await getTokenFromCode(code);

        if (!tokens) {
            res.status(400).json({ error: true, msg: 'bad request' });
            return;
        }

        const { access_token, refresh_token, id_token, expires_at } = tokens;
        const user = jwt.decode(id_token);

        const outlookConnection = new OutlookConnection({
            access_token,
            refresh_token,
            user_id: req.session.userId,
            outlook_user_name: user.name,
            token_expires: expires_at.getTime(),
            outlook_user_email: user.preferred_username,
        });

        await outlookConnection.save();

        res.redirect(process.env.SUCCESS_CALLBACK_URL);
    } catch (e) {
        console.error(e);
        res.status(500).json({ error: true, msg: 'bad request' });
    }
};
module.exports = callback;
