const authHelper = require('../../../helper/auth');

const { getToken, getUserIdByAuth } = require('../../../helper/internalAuth');

const getAuthUrl = async (req, res) => {
  const token = await getToken(req);
  if (!token) {
    res.status(400).json({ error: true, msg: 'bad request' });
    return;
  }

  const userId = await getUserIdByAuth(token);

  if (!userId) {
    res.status(400).json({ error: true, msg: 'bad request' });
    return;
  }

  req.session.userId = userId;

  const link = authHelper.getAuthUrl();
  res.json({ link });
};
module.exports = getAuthUrl;
