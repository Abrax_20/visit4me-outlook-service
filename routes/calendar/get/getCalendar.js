const graph = require('@microsoft/microsoft-graph-client');

const getCalendar = async (req, res) => {
  try {
    // Initialize Graph client
    const client = graph.Client.init({
      authProvider: (done) => {
        done(null, req.outlookUser.access_token);
      }
    });

    const start = new Date(new Date().setHours(0,0,0));
    const end = new Date(new Date(start).setDate(start.getDate() + 7));

      const response = await client
        .api(`/me/calendarView?startDateTime=${start.toISOString()}&endDateTime=${end.toISOString()}`)
        .top(10)
        .select('subject,start,end,attendees')
        .orderby('start/dateTime DESC')
        .get();

      if (!response) {
        res.json([]);
        return;
      }

      res.json(response.value);
  } catch (e) {
    console.error(e);
    res.status(500).json({ error: true, msg: 'internal server error' })
  }
};
module.exports = getCalendar;
