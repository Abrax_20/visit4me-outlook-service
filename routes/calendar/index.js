const { Router } = require('express');

const getCalendar = require('./get/getCalendar');
const addNewTerminToCalendar = require('./post/addNewTerminToCalendar');

const calendar = Router();

calendar.get('/', getCalendar);
calendar.post('/', addNewTerminToCalendar);

module.exports = calendar;
