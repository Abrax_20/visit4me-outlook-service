const graph = require('@microsoft/microsoft-graph-client');

const { bodyValue } = require('./../../../validations/request');
const { checkString, checkObject, checkArray, checkObjectValue } = require('./../../../validations/utility');


const addNewTerminToCalendar = async (req, res) => {
  try {
    const end = bodyValue('end', req, checkString);
    const start = bodyValue('start', req, checkString);
    const subject = bodyValue('subject', req, checkString);
    const emails = bodyValue('emails', req, checkArray) || [];
    const content = bodyValue('content', req, checkString) || '';
    const location = bodyValue('location', req, checkString) || '';

    if (
      end === null ||
      start === null ||
      subject === null
    ) {
      res.status(400).json({ error: true, msg: 'bad request' });
      return;
    }


    const request = {
      subject,
      body: content !== null ? {
        content,
        contentType: 'HTML',
      } : {},
      start: {
        dateTime: start,
        timeZone: 'Pacific Standard Time',
      },
      end: {
        dateTime: end,
        timeZone: 'Pacific Standard Time',
      },
      attendees: [],
      location: location !== null ? {
        displayName: location
      } : {},
    };

    for (const email of emails) {
      request.attendees.push({
        emailAddress: {
          address: email
        },
        type: 'required'
      });
    }

    const client = graph.Client.init({
      authProvider: (done) => {
        done(null, req.outlookUser.access_token);
      }
    });

    await client
      .api('/me/events')
      .post(request);

    res.json({ success: true });
  } catch (e) {
    console.error(e);
    res.status(500).json({ error: true, msg: 'internal server error' })
  }
};
module.exports = addNewTerminToCalendar;
