const graph = require('@microsoft/microsoft-graph-client');

const getCalendar = async (req, res) => {
  try {
    // Initialize Graph client
    const client = graph.Client.init({
      authProvider: (done) => {
        done(null, req.outlookUser.access_token);
      }
    });

    const response = await client
      .api('/me/contacts')
      .top(10)
      .orderby('givenName ASC')
      .get();

    if (!response) {
      res.json([]);
      return;
    }

    res.json(response.value);
  } catch (e) {
    console.error(e);
    res.status(500).json({ error: true, msg: 'internal server error' })
  }
};
module.exports = getCalendar;
