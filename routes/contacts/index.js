const { Router } = require('express');

const addContact = require('./post/addContact');
const getContacts = require('./get/getContacts');

const contacts = Router();

contacts.get('/', getContacts);
contacts.post('/', addContact);

module.exports = contacts;
