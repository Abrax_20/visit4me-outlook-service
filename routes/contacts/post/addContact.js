const graph = require('@microsoft/microsoft-graph-client');

const { bodyValue } = require('./../../../validations/request');
const { checkString, checkObject, checkArray, checkObjectValue } = require('./../../../validations/utility');

const addContact = async (req, res) => {
  try {
    const contacts = bodyValue('contacts', req, checkArray);

    if (contacts === null) {
      res.status(400).json({ error: true, msg: 'bad request' });
      return;
    }

    const client = graph.Client.init({
      authProvider: (done) => {
        done(null, req.outlookUser.access_token);
      }
    });

    for (const contact of contacts) {
      const request = {};

      const givenName = checkObjectValue('firstname', contact, checkString);
      if (givenName === null)
        break;

      const emailAddresses = [];
      const businessPhones = [];
      const department = checkObjectValue('role', {}) || '';
      const company = checkObjectValue('company', contact, checkObject) || {};
      const surname = checkObjectValue('lastname', contact, checkString) || '';
      const services = checkObjectValue('services', contact, checkArray) || [];

      for (const service of services) {
        if (service.type === 'email') {
          emailAddresses.push({
            address: service.data.email,
            name: `${givenName} ${surname}`,
          })
        }
        if (services.type === 'phone-number') {
          businessPhones.push(service.data['phone-number']);
        }
      }

      await client
        .api('/me/contacts')
        .post({
          surname,
          givenName,
          department,
          emailAddresses,
          businessPhones,
          companyName: company.title || ''
        });
    }

    res.json({ success: true });
  } catch (e) {
    console.error(e);
    res.status(500).json({ error: true, msg: 'internal server error' })
  }
};
module.exports = addContact;
