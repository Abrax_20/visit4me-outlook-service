const graph = require('@microsoft/microsoft-graph-client');

const getEmails = async (req, res) => {
  try {
    const client = graph.Client.init({
      authProvider: (done) => {
        done(null, req.outlookUser.access_token);
      }
    });

    const response = await client
      .api('/me/mailfolders/inbox/messages')
      .top(10)
      .select('subject,from,receivedDateTime,isRead')
      .orderby('receivedDateTime DESC')
      .get();

    if (!response) {
      res.json([]);
      return;
    }

    res.json(response);
  } catch (e) {
    console.error(e);
    res.status(500).json({ error: true, msg: 'internal server error' })
  }
};
module.exports = getEmails;
