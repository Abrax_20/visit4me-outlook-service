const { Router } = require('express');

const getEmails = require('./get/getEmails');
const sendMail = require('./post/sendMail.js');

const emails = Router();

emails.get('/', getEmails);
emails.post('/send', sendMail);

module.exports = emails;
