const graph = require('@microsoft/microsoft-graph-client');

const sendMail = async (req, res) => {
  try {
    const { subject, message, to, cc } = req.body;

    if (
      !to ||
      !subject ||
      !message ||
      typeof subject !== 'string' ||
      typeof message !== 'string'
    ) {
      res.status(400).json({ error: true, msg: 'bad request' });
      return;
    }

    const client = graph.Client.init({
      authProvider: (done) => {
        done(null, req.outlookUser.access_token);
      }
    });

    const request ={
      message: {
        subject,
        body: {
          contentType: 'HTML',
          content: message
        },
        toRecipients: to.map(receiver => ({emailAddress: {address: receiver}})),
        ccRecipients: cc ? cc.map(receiver => ({emailAddress: {address: receiver}})) : [],
      },
      saveToSentItems: 'false'
    };

    await client
      .api('/me/sendMail')
      .post(request);

    res.json({ success: true });
  } catch (e) {
    console.error(e);
    res.status(500).json({ error: true, msg: 'internal server error' })
  }
};
module.exports = sendMail;
