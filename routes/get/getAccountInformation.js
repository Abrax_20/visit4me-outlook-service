const getAccountInformation = async (req, res) => {
  try {
    res.json({
      user_id: req.outlookUser.user_id,
      company_id: req.outlookUser.company_id,
      outlook_user_name: req.outlookUser.outlook_user_name,
    });
  } catch (e) {
    console.error(e);
    res.status(500).json({ error: true, msg: 'internal server error' })
  }
};
module.exports = getAccountInformation;
