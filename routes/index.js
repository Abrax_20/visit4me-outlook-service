const { Router } = require('express');

const { checkAuthentication, getOutlookUser } = require('./../controller/auth');
const { checkAccessToken } = require('./../controller/oauth2');

const getAccountInformation = require('./get/getAccountInformation');

const auth = require('./auth');
const emails = require('./emails');
const calendar = require('./calendar');
const contacts = require('./contacts');

const index = Router();

index.use('/auth', auth);

index.use(checkAuthentication);
index.use(getOutlookUser);

index.get('/', getAccountInformation);

index.use(checkAccessToken);

index.use('/emails', emails);
index.use('/calendar', calendar);
index.use('/contacts', contacts);

module.exports = index;
