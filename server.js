require('dotenv').config();
const cors = require('cors');
const morgan = require('morgan');
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cookieSession = require('cookie-session');

const routes = require('./routes');

const app = express();
const port = process.env.PORT || 3000;

mongoose.connect(`mongodb://mongo/visit4me_outlook`, { useNewUrlParser: true }).catch(err => console.log(err));

app.use(cors());
app.set('trust proxy', 1);
app.use(cookieParser());
app.use(cookieSession({
    proxy: true,
    resave: false,
    saveUninitialized: true,
    key: 'outlook_session.sid',
    secret: process.env.SESSION || 'S3CRE7',
    cookie: {
        path: '/',
        secure: true,
        domain: '.visit4.me',
        maxAge: 60 * 24
    }
}));
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(routes);

app.get('/', function (req, res) {
    res.send('Office API is Running!');
});

app.listen(port, () => {
    console.log('Outlook service started on port ' + port);
});
