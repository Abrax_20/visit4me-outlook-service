// @flow
const bodyValue = (
  value,
  req,
  checker,
  options = {}
) => {
  let requestValue = null;

  if (req.body && value instanceof Array) {
    let objectValue = {};
    for(let x=0;value.length > x;x++) {
      if (x == 0) {
        if (!req.body[value[x]]) {
          return null;
        }
        objectValue = req.body[value[x]];
      } else {
        if (!objectValue[value[x]]) {
          return null;
        }

        objectValue = objectValue[value[x]]
      }
    }

    requestValue = checker(objectValue);
  } else {
    if (!req.body && !req.body[value]) {
      return null;
    }

    requestValue = checker(req.body[value]);
  }

   return requestValue;
};
exports.bodyValue = bodyValue;

const paramsValue = (
  value,
  req,
  checker = value => value
) => {
  let requestValue = null;
  if (!req.params && !req.params[value]) {
    return null;
  }

  requestValue = checker(req.params[value]);


   return requestValue;
};
exports.paramsValue = paramsValue;

const queryValue = (
  value,
  req,
  checker = value => value
) => {
  let requestValue = null;

  if (!req.query && !req.query[value]) {
    return null;
  }

  requestValue = checker(req.query[value]);

   return requestValue;
};
exports.queryValue = queryValue;
