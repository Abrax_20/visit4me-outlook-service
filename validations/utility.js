// @flow
const mongoose = require('mongoose');

const boolean = value  => {
  if (typeof value !== 'boolean') {
    return null;
  }

  return value;
};
exports.checkBoolean = boolean;

const number = value => {
  if (typeof value !== 'number') {
    if (parseInt(value))
      return parseInt(value);
    return null;
  }

  return value;
};
exports.checkNumber = number;


const string = value => {
  if (typeof value !== 'string' && !!value) {
    return null;
  }

  return value;
};
exports.checkString = string;

const object = value => {
  if (typeof value !== 'object') {
    return null;
  }

  return value;
};
exports.checkObject = object;

const array = value => {
  if (value instanceof Array
      || Array.isArray(value)
      || (value && value !== Object.prototype && array(value.__proto__))
     ) {
       return value;
    }

    return null;
};
exports.checkArray = array;

const objectId = value => {
  if (string(value) === null ||
      !mongoose.Types.ObjectId.isValid(value)) {
    return null;
  }

  return value;
};
exports.checkObjectId = objectId;

const objectValue = (
  value,
  object,
  checker = value => value
) => {
  let requestValue = null;

  if (object && value instanceof Array) {
    let objectValue = {};
    for(let x=0;value.length > x;x++) {
      if (x == 0) {
        if (!object[value[x]]) {
          return null;
        }
        objectValue = object[value[x]];
      } else {
        if (!objectValue[value[x]]) {
          return null;
        }

        objectValue = objectValue[value[x]]
      }
    }

    requestValue = checker(objectValue);
  } else {
    if (!object && !object[value]) {
      return null;
    }

    requestValue = checker(object[value]);
  }

  return requestValue;
};
exports.checkObjectValue = objectValue;
